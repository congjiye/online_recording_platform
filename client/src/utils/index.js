export function Base64ToBlob(urlData, type = 'audio/wav') {
    try {
        let arr = urlData.split(',')
        let mime = arr[0].match(/:(.*?);/)[1] || type

        // data
        let bytes = window.atob(arr[1])
        let ab = new ArrayBuffer(bytes.length)
        let ia = new Uint8Array(ab)

        for (let i = 0; i < bytes.length; i++) {
            ia[i] = bytes.charCodeAt(i)
        }

        return new Blob([ab], {
            type: mime
        })
    } catch (e) {
        let ab = new ArrayBuffer(0)
        return new Blob([ab], {
            type: type
        })
    }
}

export function Base64ToURL(urlData) {
    if (urlData !== "") {
        let blob = Base64ToBlob(urlData)
        return URL.createObjectURL(blob)
    } else {
        return urlData
    }
}

export function guid() {
    function S4() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    }
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4()) + '.wav';
}