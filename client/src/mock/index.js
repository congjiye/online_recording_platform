import Mock from 'mockjs'

const data = {
    "src": "",
    "filename": "",
    "data": '@ctitle'
}

Mock.mock('/api/data', 'get', data)

export default Mock;