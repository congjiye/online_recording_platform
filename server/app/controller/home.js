'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
  }

  async uploadText() {
    const { ctx } = this;
    const form = ctx.request.body;
  }
}

module.exports = HomeController;
