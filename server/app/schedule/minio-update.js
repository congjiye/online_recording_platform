'use strict';

const Minio = require('minio');
const Subscription = require('egg').Subscription;

class UpdateBucket extends Subscription {
  static get schedule() {
    return {
      cron: '0 0 0 * * *',
      type: 'all',
    };
  }

  async task() {
    const minioClient = new Minio.Client({
      endpoint: 'your-minio-url',
      port: 8080,
      useSSL: true,
      accessKey: 'your-access-key',
      secretKey: 'your-secret-key',
    });
    const date = new Date();
    const today = `${date.getFullYear()}/${date.getMonth()}/${date.getDay()}`;
    minioClient.bucketExists(today, function(err) {
      if (err) {
        if (err.code === 'NoSuchBucket') {
          minioClient.makeBucket(today, 'cn-north-1', function(err) {
            if (err) {
              return console.error('Error creating bucket', err);
            }
            console.log('Bucket create successfully in "cn-north-1"');
          });
        }
        return console.log(err);
      }
    });

  }
}

module.exports = UpdateBucket;
