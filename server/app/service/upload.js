'use strict';

const Service = require('egg').Service;
const Minio = require('minio');
const Pump = require('mz-modules/pump');


class UploadService extends Service {
  async saveFileToBucket() {
    const minioClient = new Minio.Client({
      endpoint: 'your-minio-url',
      port: 8080,
      useSSL: true,
      accessKey: 'your-access-key',
      secretKey: 'your-secret-key',
    });
    return {
      status: true,
    };
  }
}

module.exports = UploadService;
